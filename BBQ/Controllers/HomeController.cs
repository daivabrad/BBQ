﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BBQ.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult MyHome()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Participation()
        {         
            return View();
        }

        [HttpPost]
        //kaip parasom Participation vietoj Confirmation, reikia prideti HttpPost ir HttpGet

        public ActionResult Participation(string name, string email, string gsm, string choice, int number=0)
        {
            if(String.IsNullOrEmpty(name) || String.IsNullOrEmpty(email) || !email.Contains("@") || number<=0 || number>10)
            {
                ViewBag.Erreur = "Please fill in the form correctly!";
                return View();
               
            }

            bool ? switchImage = null;

            switch (choice)
            {
                 
        case "yes":
                 switchImage = true;
                 ViewBag.Message = "Welcome to the party!";
                break;
        case "no":
                    switchImage = false;
                    ViewBag.Message = "Dommage!";break;
         default:
                 ViewBag.Message = "Next time perhaps?"; break;
                
            }
            ViewBag.switchImage = switchImage;
            return View("Confirmation");

        }
        

            //if (choice == "yes")
            //{
            //    image = "cat.jpg";
            //    message = "Welcome to the party!";
            //}

            //if (choice == "no")
            //{
            //    message = "Dommage!";
            //}
            //if (choice == "dontknow")
            //{
            //    message = "Next time perhaps?";
            //}
            //ViewBag.choice = message;
            //ViewBag.choice = image;
            
        

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}